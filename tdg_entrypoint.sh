#!/bin/bash -e

echo HalloWelt

: "${TDG_PROTOCOL:=http}"
: "${TDG_HOST:=localhost}"
: "${TDG_PORT:=:3000}"
: "${TDG_REST:=/rest/}"

if [ -e /usr/share/nginx/html/app.*.js ]; then
    sed -e "s|TDG_PROTOCOL|$TDG_PROTOCOL|g;\
    s|TDG_HOST|$TDG_HOST|g;\
    s|TDG_PORT|$TDG_PORT|g;\
    s|TDG_REST|$TDG_REST|g"\
    /usr/share/nginx/html/app.*.js\
    > /usr/share/nginx/html/app-temp.js
    cd /usr/share/nginx/html/
    mv app-temp.js app.*.js
fi

exec nginx -g "daemon off;"