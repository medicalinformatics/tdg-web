FROM node:11.7.0 AS build

ADD . /build

WORKDIR /build

RUN npm set proxy http://www-int2.dkfz-heidelberg.de:3128
RUN npm set https-proxy http://www-int2.dkfz-heidelberg.de:3128

RUN npm install
RUN npm run build


FROM nginx

RUN apt update && apt install dos2unix
COPY --from=build /build/dist /usr/share/nginx/html
COPY ./tdg_entrypoint.sh /tdg_entrypoint.sh
RUN dos2unix /tdg_entrypoint.sh

ENTRYPOINT ["./tdg_entrypoint.sh"]
