import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Property} from '../model/property';
import {Profile} from '../model/profile';
import {ConfigService} from "../config/service";

@Injectable()
export class ProfileService{
    urlProfile : string;

    constructor(private http: HttpClient, public configService: ConfigService){
        this.urlProfile = configService.getUrlPath();
    }


    getProfile(profileName: string){
        let profile : Profile;
        let url : string = (profileName != null) ? ( this.urlProfile + 'profile/get' + profileName) : this.urlProfile+ 'profile/get';
        return this.http.get(url);
    } 

    getProfileNames(){
        let profileNames: String[] = [];
        return this.http.get(this.urlProfile + 'profile/getNames');
    }
    
    safeProfile(profileName: string, profileDescription: string, properties: Property[]){
        let body = {
            "profileName": profileName,
            "profileDescription": profileDescription,
            "probabilities": this.convertPropertiesToProbabilityJSON(properties)
        }
        console.log(body);
        return this.http.put(this.urlProfile + 'profile/safeProfile', body);
    }

    getPropertyDefaultValue(propertyName: string){
        return this.http.get(this.urlProfile + 'profile/getPropertyDefault' + propertyName);
    }

    convertPropertiesToProbabilityJSON(properties: Property[]): any[]{
        let probabilityJSON: any[] = [];
        properties.forEach(property => {
            probabilityJSON.push({
                "name": property.pname,
                "section": property.psection,
                "group": property.pgroup,
                "probability": property.probability,
                "modifiedFlag": property.isModified,
            })
        });
        return probabilityJSON;
    };
}