let defaultConfig = require("../../assets/deployment/defaultConfig.json");
export let config = {
     "protocol": defaultConfig.protocol,
     "host": defaultConfig.host,
     "port": defaultConfig.port,
     "rest" : defaultConfig.rest
 };
