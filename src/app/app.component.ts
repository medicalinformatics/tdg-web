import { Component, ViewChild} from '@angular/core';

import '../assets/css/styles.css';

import {Profile} from './model/profile';
import {Property} from './model/property';
import {ProfileService} from './services/profile.service';
import {GeneratorService} from './services/generator.service';
import { ProfileSelectionComponent }  from './profileselection/profileselection.component';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProfileService, GeneratorService]
})  

export class AppComponent{
  profile: Profile;
  sections: string[];
  generateAmount: number = 10;
  @ViewChild(ProfileSelectionComponent)
  profileSelectionComponent: ProfileSelectionComponent;

  constructor(private profileService: ProfileService, private generatorService: GeneratorService){
    profileService.getProfile(null).subscribe(data => {
      let profileProperties : Property[] = [];
      for(let key in data['probabilityHashMap']){
        let property: Property = {
          pname: key,
          probability: data['probabilityHashMap'][key]['probability'],
          psection: data['probabilityHashMap'][key]['section'],
          pgroup: data['probabilityHashMap'][key]['group'],
          isModified: data['probabilityHashMap'][key]['modifiedFlag'] 
        };
        profileProperties.push(property);
      }
      this.profile = {
        name: 'cancerProfile',
        properties: profileProperties
      }
      this.fillSections(this.profile.properties);
    })
  }

  fillSections(properties: Property[]){
    let sections: string[] = [];
    properties.forEach(property => {
      if(sections.indexOf(property.psection) == -1)
        sections.push(property.psection);
    });
    this.sections = sections;
  }

  propertiesForSection(section: string): Property[]{
    let sectionProperties: Property[] = [];
    this.profile.properties.forEach(property => {
      if(property.psection == section){
        sectionProperties.push(property);
      }
    });
    return sectionProperties;
  }

  handleProfileChange(newProfile : string){
    console.log("Profile changed. App Component now processing new Profile: " + newProfile)
    if(newProfile !== this.profile.name){
      this.profile.name = newProfile;
      this.profileService.getProfile(newProfile).subscribe(data => {
        let newProperties : Property[] = [];
        for(let key in data['probabilityHashMap']){
          let property: Property = {
            pname: key,
            probability: data['probabilityHashMap'][key]['probability'],
            psection: data['probabilityHashMap'][key]['section'],
            pgroup: data['probabilityHashMap'][key]['group'],
            isModified: data['probabilityHashMap'][key]['modifiedFlag'] 
          };
          newProperties.push(property); 
        }
        this.profile.properties = newProperties;
      })
    }
      
  }

  generatePatients(){
    this.generatorService.generatePatientsFromProperties(this.profile.properties, this.generateAmount).subscribe(data => {});
  }

  handlePropertyValueChange(newProperty: Property): void{
    let property: Property = this.profile.properties.find((property: Property) => {
      return property.pname === newProperty.pname;
    });
    property.probability = newProperty.probability;
    this.profileService.getPropertyDefaultValue(property.pname).subscribe(data => {
      if(property.probability == data){
        property.isModified = false;
      } else {
        property.isModified = true;
      }
    })
  }

  handleProfileSafe(): void{
    this.profileSelectionComponent.getProfileNames();
  }

}
