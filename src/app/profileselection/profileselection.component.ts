import {
    Component,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    AfterViewInit
} from '@angular/core';

import { ProfileService} from '../services/profile.service';
import { GeneratorService } from '../services/generator.service';

@Component({
    selector: 'profile-selection',
    templateUrl: './profileselection.component.html',
    providers: [ProfileService, GeneratorService]
})
export class ProfileSelectionComponent  implements AfterViewInit {
    profileNames: String[];
    @Input() generateAmount: number;
    @ViewChild('profileName') select: ElementRef;
    //  @ViewChild('profileName') select: ChildViewComponent;
    @Output() onProfileChange = new EventEmitter<String>();

    constructor(private profileSelection: ProfileService, private generatorService: GeneratorService) {
        this.getProfileNames();
    }

    ngAfterViewInit() {
        console.log("executing onChangeProfile after startup");
        this.onChangeProfile();
    }

    onChangeProfile(profileName?: String): void {
        console.log("onChangeProfile called with parameter: " + profileName);
        if(profileName != undefined) {
            console.log("onChangeProfile with Parameter. Setting profile to " + profileName);
            this.onProfileChange.emit(profileName);
        } else{
            console.log("onChangeProfile with no Parameter. Setting profile to " + this.select.nativeElement[0].value)
            this.onProfileChange.emit(this.select.nativeElement[0].value)
        }
    }

    generatePatients(): void {
        console.log(this.select.nativeElement.value)
        this.generatorService.generatePatients(this.select.nativeElement.value, this.generateAmount).subscribe(data => {
            console.log(data);
        })
    }

    getProfileNames(): void {
        this.profileSelection.getProfileNames().subscribe(data => {
            if(data instanceof Array){
                this.profileNames = data as String[];
            }
        });
    }
}