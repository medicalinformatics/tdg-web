New features:
- Dockerfile: An image which contains all frontend information
- docker-compose.yml: It starts the different images (backend, frontend and database) and reserves all ports. It also reserves a port to debug inside the tomcat container
- tdg_entrypoint.sh: It's possible to revise the app.*.js file. This Node.js creates this file.
- docker-compose.override.yml: It contains all local ports and environment variables.
